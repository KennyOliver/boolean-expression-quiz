# boolean-expression-quiz

![CodeFactor](https://www.codefactor.io/repository/github/KennyOliver/boolean-expression-quiz/badge?style=for-the-badge)
![Latest SemVer](https://img.shields.io/github/v/tag/KennyOliver/boolean-expression-quiz?label=version&sort=semver&style=for-the-badge)
![Repo Size](https://img.shields.io/github/repo-size/KennyOliver/boolean-expression-quiz?style=for-the-badge)
![Total Lines](https://img.shields.io/tokei/lines/github/KennyOliver/boolean-expression-quiz?style=for-the-badge)

[![repl](https://replit.com/badge/github/KennyOliver/boolean-expression-quiz)](https://replit.com/@KennyOliver/boolean-expression-quiz)

**Quiz yourself on Boolean expressions**

## VividHues :rainbow: :package:
**boolean-expression-quiz** uses **VividHues** - my own Python Package!

[![VividHues](https://img.shields.io/badge/Get%20VividHues-252525?style=for-the-badge&logo=python&logoColor=white&link=https://github.com/KennyOliver/VividHues)](https://github.com/KennyOliver/VividHues)

---
Kenny Oliver ©2021
